<!DOCTYPE html>
<html lang="en">

<style type="text/css">
    .tg {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .tg td {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg th {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg .tg-zv4m {
        border-color: #ffffff;
        text-align: left;
        vertical-align: top
    }

    .tg .tg-sdug {
        border-color: #ffffff;
        font-size: 18px;
        text-align: center;
        vertical-align: middle
    }

    .tg_detail {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .tg_detail td {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg_detail th {
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }

    .tg_detail .tg-cly1 {
        text-align: left;
        vertical-align: middle
    }

    .tg_detail .tg-mwxe {
        text-align: right;
        vertical-align: middle
    }

    .tg_detail .tg-baqh {
        text-align: center;
        vertical-align: top
    }

    .tg_detail .tg-lvth {
        font-size: 16px;
        text-align: center;
        vertical-align: top
    }

    .tg_detail .tg-0lax {
        text-align: left;
        vertical-align: top
    }
</style>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>Generate PDF CodeIgniter 4 - qadrLabs</title>

</head>

<body>
    <div style="padding: 20px;">
        <a href="<?php echo base_url('post/') ?>" class="btn btn-md btn-primary mb-3">HOME</a>
        &nbsp;
        <a href="<?php echo site_url('pdf/generate') ?>" class="btn btn-md btn-dark mb-3">Download PDF</a>
    </div>
    <table class="tg" style="undefined;table-layout: fixed; width: 1068px">
        <colgroup>
            <col style="width: 188.333333px">
            <col style="width: 103.333333px">
            <col style="width: 322.333333px">
            <col style="width: 454.333333px">
        </colgroup>
        <thead>
            <tr>
                <td class="tg-zv4m" rowspan="2"><img src="<?php echo base_url('assets/images/logo.png'); ?>" width="100px" height="50px"></td>
                <td class="tg-sdug" colspan="2" rowspan="2"><span style="font-weight:bold;text-decoration:underline">BUKTI BANK MASUK</span><br></td>
                <td class="tg-zv4m">TANGGAL: 05/01/2024 [CETAK KE 2]</td>
            </tr>
            <tr>
                <td class="tg-zv4m">NO. BUKTI: 061/BBM/01/24/LHK</td>
            </tr>
        </thead>
    </table>
    <br>
    <table class="tg_detail" style="undefined;table-layout: fixed; width: 1067px">
        <colgroup>
            <col style="width: 210.333333px">
            <col style="width: 208.333333px">
            <col style="width: 425.333333px">
            <col style="width: 223.333333px">
        </colgroup>
        <thead>
            <tr>
                <th class="tg-lvth"><span style="font-weight:bold">JENIS</span></th>
                <th class="tg-lvth"><span style="font-weight:bold">NAMA BANK</span></th>
                <th class="tg-baqh"><span style="font-weight:bold">URAIAN</span></th>
                <th class="tg-lvth"><span style="font-weight:bold">JUMLAH</span></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($posts as $key => $post) : ?>
                <tr>
                    <td class="tg-cly1"><?php echo $post['jenis'] ?></td>
                    <td class="tg-cly1"><?php echo $post['bankName'] ?></td>
                    <td class="tg-cly1"><?php echo $post['description'] ?></td>
                    <td class="tg-mwxe"><?php echo rupiah($post['amount']) ?><br></td>
                </tr>
            <?php endforeach ?>
            <tr>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
                <td class="tg-mwxe"><span style="font-weight:bold">GRAND TOTAL:</span></td>
                <td class="tg-mwxe"><span style="font-weight:bold">
                        <?php
                        $db      = \Config\Database::connect();
                        $builder = $db->table('posts');
                        $builder->select('(SELECT SUM(posts.amount) FROM posts) AS amount_paid', false);
                        $query = $builder->get();
                        $row = $query->getRow();
                        if (isset($row)) {
                            echo rupiah($row->amount_paid);
                        }
                        ?></span></td>
            </tr>
        </tbody>
    </table>
</body>

</html>