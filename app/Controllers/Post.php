<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\PostModel;

class Post extends Controller
{
    /**
     * index function
     */
    public function index()
    {
        //model initialize
        $postModel = new PostModel();

        //pager initialize
        $pager = \Config\Services::pager();

        $data = array(
            'posts' => $postModel->paginate(2, 'post'),
            'pager' => $postModel->pager
        );

        return view('post-index', $data);
    }

    /**
     * create function
     */
    public function create()
    {
        return view('post-create');
    }

    /**
     * store function
     */
    public function store()
    {
        //load helper form and URL
        helper(['form', 'url']);

        //define validation
        $validation = $this->validate([
            'jenis' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Jenis Post.'
                ]
            ],
            'bankName'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Nama Bank Post.'
                ]
            ],
            'description'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Uraian Post.'
                ]
            ],
            'amount'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Jumlah Post.'
                ]
            ],
        ]);

        if (!$validation) {

            //render view with error validation message
            return view('post-create', [
                'validation' => $this->validator
            ]);
        } else {

            //model initialize
            $postModel = new PostModel();

            //insert data into database
            $postModel->insert([
                'jenis'   => $this->request->getPost('jenis'),
                'bankName' => $this->request->getPost('bankName'),
                'description' => $this->request->getPost('description'),
                'amount' => $this->request->getPost('amount'),
            ]);

            //flash message
            session()->setFlashdata('message', 'Post Berhasil Disimpan');

            return redirect()->to(base_url('post'));
        }
    }

    /**
     * edit function
     */
    public function edit($id)
    {
        //model initialize
        $postModel = new PostModel();

        $data = array(
            'post' => $postModel->find($id)
        );

        return view('post-edit', $data);
    }

    /**
     * update function
     */
    public function update($id)
    {
        //load helper form and URL
        helper(['form', 'url']);

        //define validation
        $validation = $this->validate([
            'jenis' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Jenis Post.'
                ]
            ],
            'bankName'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Nama Bank Post.'
                ]
            ],
            'description'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Uraian Post.'
                ]
            ],
            'amount'    => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukkan Jumlah Post.'
                ]
            ],
        ]);

        if (!$validation) {

            //model initialize
            $postModel = new PostModel();

            //render view with error validation message
            return view('post-edit', [
                'post' => $postModel->find($id),
                'validation' => $this->validator
            ]);
        } else {

            //model initialize
            $postModel = new PostModel();

            //insert data into database
            $postModel->update($id, [
                'jenis'   => $this->request->getPost('jenis'),
                'bankName' => $this->request->getPost('bankName'),
                'description'   => $this->request->getPost('description'),
                'amount' => $this->request->getPost('amount'),
            ]);

            //flash message
            session()->setFlashdata('message', 'Post Berhasil Diupdate');

            return redirect()->to(base_url('post'));
        }
    }

    /**
     * delete function
     */
    public function delete($id)
    {
        //model initialize
        $postModel = new PostModel();

        $post = $postModel->find($id);

        if ($post) {
            $postModel->delete($id);

            //flash message
            session()->setFlashdata('message', 'Post Berhasil Dihapus');

            return redirect()->to(base_url('post'));
        }
    }
}
