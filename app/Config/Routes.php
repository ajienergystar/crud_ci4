<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->setAutoRoute(true);
$routes->get('/post', 'Post::index');
$routes->get('/post/create', 'Post::create');
$routes->post('/post/store', 'Post::store');
$routes->get('/post/edit', 'Post::edit');
$routes->post('/post/update', 'Post::update');
$routes->post('/post/delete', 'Post::delete');

$routes->get('/post/report', 'PdfController::index');
$routes->get('/pdf/generate', 'PdfController::generate');
