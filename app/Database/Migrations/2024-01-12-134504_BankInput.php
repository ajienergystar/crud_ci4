<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class BankInput extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'           => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'jenis'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255'
            ],
            'bankName'     => [
                'type'           => 'VARCHAR',
                'constraint'     => '100'
            ],
            'description'     => [
                'type'           => 'VARCHAR',
                'constraint'     => '255'
            ],
            'amount'     => [
                'type'           => 'INT',
                'constraint'     =>  11
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('posts');
    }

    public function down()
    {
        //
    }
}
